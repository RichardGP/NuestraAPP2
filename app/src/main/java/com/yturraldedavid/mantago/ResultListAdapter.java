package com.yturraldedavid.mantago;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ResultListAdapter extends RecyclerView.Adapter<ResultListAdapter.ViewHolderLugares>
    implements View.OnClickListener{

    ArrayList<LugaresTuristicos> listaLugares;
    private  View.OnClickListener listener;

    public ResultListAdapter(ArrayList<LugaresTuristicos> listaLugares) {
        this.listaLugares = listaLugares;
    }

    @NonNull
    @Override
    public ViewHolderLugares onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_result_row, parent, false);
        view.setOnClickListener(this);
        return new ViewHolderLugares(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderLugares holder, int position) {
        holder.TextViewNombreLugar.setText(listaLugares.get(position).getNombre());
        holder.ImageViewLugar.setImageResource(listaLugares.get(position).getFoto());
    }

    @Override
    public int getItemCount() {
        return listaLugares.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener= listener;
    }
    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);

        }
    }

    public class ViewHolderLugares extends RecyclerView.ViewHolder {
        ImageView ImageViewLugar;
        TextView TextViewNombreLugar;
        public ViewHolderLugares(View itemView) {
            super(itemView);
            ImageViewLugar = (ImageView) itemView.findViewById(R.id.imageViewLugar);
            TextViewNombreLugar = (TextView) itemView.findViewById(R.id.textViewNombreLugar);
        }
    }
}
